# Разные полезности

Актуальная информация: http://wiki.leolab.info/lib/appconf.go/

## Функции

### GetCDW
```
func GetCWD() string {}
```
Получить текущую рабочую папку

### GetBinDir
```
func GetBinDir() string {}
```
Получить папку исполняемого файла

### GetBinName
```
func GetBinName() string {}
```
Получить имя исполняемого файла

### FileExists
```
func FileExists(string) bool {}
```
Проверить наличие файла.  
Более колротка замена "GO Way".

```
if FileExists(fName) {
  // Файл существует
}else{
  // Файла нет
}
```

### FindFile
```
func FindFile([]string) (string,error){}
```
Найти файл из списка.  
Функция возвращает первый найденный файл из переданного массива

```
if fName,err := FindFile([]string{"file1","file2"}); err!=nil{
  if err == ENotFound {
    // Ни одного файла не найдено
  }else{
    // Другая ошибка (?)
  }
}

```

### ParseArgs
```
func ParseArgs() (map[string]string,[]string)
```
Разобрать параметры запуска.  
Параметры (ключи) запуска ожидаются в POSIX-Like форме:
```
cmd -k --key --key2=value command1
```
Одиночные ключи можно объединять в один блок:
```
cmd -kot
// -k -o -t
```

```
args,cmds := ParseArgs()
// в args - ключи
// в cmds - команды
```

### ImplodeArgs
```
func ImplodeArgs(map[string]string,[]string) []string {}
```
Собрать параметры запуска

## Классы

### TIniFile
```
type TIniFile struct {}

func NewIniFile(string) (*TIniFile,error){}

func (*TIniFile) Parse() error {}

func (*TIniFile) HasSection(string) bool {}

func (*TIniFile) GetSecriot(string) map[string]string {}

func (*TIniFile) HasValue(string,string) bool {}

func (*TIniFile) GetValue(string,string) string {}

```