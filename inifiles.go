// Package leotools - inifiles
package leotools

import (
	"bufio"
	"errors"
	"io"
	"os"
	"strings"
)

// IniFile -
type IniFile struct {
	fileName string
	Data     map[string]map[string]string
}

// NewIniFile -
func NewIniFile(fName string) (f *IniFile, err error) {
	if !FileExists(fName) {
		return nil, &EFileNotFound{errors.New("File not found: " + fName)}
	}
	f = &IniFile{
		fileName: fName,
		Data:     make(map[string]map[string]string),
	}
	err = f.Parse()
	if err != nil {
		return nil, err
	}
	return f, nil
}

// Parse -
func (i *IniFile) Parse() error {
	var (
		cs   string
		s    string
		err  error
		f    *os.File
		k, v string
	)
	f, err = os.Open(i.fileName)
	if err != nil {
		return err
	}
	defer f.Close()
	r := bufio.NewReader(f)
	for {
		if s, err = r.ReadString('\n'); err != nil {
			if err == io.EOF {
				break
				//				return nil
			} else {
				return err
			}
		}
		s = strings.Trim(s, " \n")
		if len(s) == 0 { // Пустая строка
			continue
		}
		switch s[0] {
		case '[': //Секция
			cs = strings.Trim(s, "[]\n") //Новая секция
			continue
		case ';', '#': //Комменты
			continue
		}
		// Параметр
		if strings.Contains(s, ";") {
			sa := strings.SplitN(s, ";", 2)
			s = sa[0]
		}
		if strings.Contains(s, "#") {
			sa := strings.SplitN(s, "#", 2)
			s = sa[0]
		}

		if strings.Contains(s, "=") { // Ключ со значением
			sa := strings.SplitN(s, "=", 2)
			k = strings.Trim(sa[0], " ")
			v = strings.Trim(sa[1], " \"\n")
		} else { //Ключ без значения
			k = s
			v = ""
		}
		if _, ok := i.Data[cs]; !ok {
			i.Data[cs] = make(map[string]string)
		}
		i.Data[cs][k] = v
	}
	return nil
}

// HasSection -
func (i *IniFile) HasSection(s string) bool {
	if _, ok := i.Data[s]; ok {
		return true
	}
	return false
}

// HasValue -
func (i *IniFile) HasValue(s string, k string) bool {
	if _, ok := i.Data[s][k]; ok {
		return true
	}
	return false
}

// GetValue -
func (i *IniFile) GetValue(s string, k string) string {
	v, _ := i.Data[s][k]
	return v
}

// GetSection -
func (i *IniFile) GetSection(s string) map[string]string {
	if i.HasSection(s) {
		return i.Data[s]
	}
	return nil
}

// LoadSection -
func (i *IniFile) LoadSection(d map[string]string, s string) error {
	if !i.HasSection(s) {
		return nil
	}
	for k, v := range i.Data[s] {
		d[k] = v
	}
	return nil
}
