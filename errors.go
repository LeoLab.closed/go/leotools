// Package leotools - errors
package leotools

/*
import (
	"errors"
)
*/
/* ENotFound */

/*
type LError struct {
	Msg string
	error
}

func (e LError) Error() string {
	return e.Msg
}
*/

// ENotFound - Не найдено
type ENotFound struct{ error }

//func (e ENotFound) Error() string { return e.Msg }

// EFileNotFound - Файл не найден
type EFileNotFound struct{ error }

/*
var (
	ENotFound     = errors.New("Не найдено")
	EFileNotFound = errors.New("Файл не найден")
)
*/
