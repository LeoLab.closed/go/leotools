// Package leotools - files
package leotools

import (
	"os"
	"path/filepath"
	"strings"
)

// GetCWD -  Получить текущую рабочую папку
func GetCWD() (wd string) {
	var err error
	if wd, err = os.Getwd(); err != nil {
		// Has error
		panic("os.Getwd: " + err.Error())
	}
	return
}

// GetBinDir - Получить папку где находится исполняемый файл
func GetBinDir() (bd string) {
	var (
		err error
		en  string
	)
	if en, err = os.Executable(); err != nil {
		panic("os.Executable: " + err.Error())
		//		return ""
	}
	if bd, err = filepath.Abs(en); err != nil {
		panic("filepath.Abs(" + en + "): " + err.Error())
	}
	return filepath.Dir(bd)
}

// GetBinName - Имя исполняемого файла
func GetBinName() (bn string) {
	var (
		err error
	)
	if bn, err = os.Executable(); err != nil {
		panic("os.Executable: " + err.Error())
	}
	return filepath.Base(bn)
}

// GetUpDir - Получить из пути папку верхнего уровня
func GetUpDir(s string) (p string) {
	return s[:strings.LastIndex(s, "/")]
}
