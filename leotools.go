// Package leotools project leotools.go
package leotools

import (
	//	"errors"
	"os"
	"strings"
)

const (
	// Version -
	Version = "0.2.3"
	// Date -
	Date = "31/05/2018"
)

// FileExists -
func FileExists(fName string) bool {
	if _, err := os.Stat(fName); os.IsNotExist(err) {
		return false
	}
	return true
}

// FindFile -
func FindFile(aFile []string) (fName string, err error) {
	for _, fName := range aFile {
		if FileExists(fName) {
			return fName, nil
		}
	}
	return "", &ENotFound{}
}

// ParseArgs -
func ParseArgs() (args map[string]string, cmds []string) {
	args = make(map[string]string)
	cmds = make([]string, 0)
	for i, s := range os.Args {
		if i == 0 {
			//			args["cmd"] = s //Первый параметр - имя запускаемого файла, пропускаем
		} else {
			if strings.HasPrefix(s, "--") { // Длинный ключ, может быть с параметром
				ts := strings.TrimPrefix(s, "--")
				// Может быть с параметром
				if strings.ContainsRune(ts, '=') {
					sa := strings.SplitN(ts, "=", 2)
					args[sa[0]] = sa[1]
				} else {
					args[ts] = ""
				}
			} else if strings.HasPrefix(s, "-") { // Короткий ключ, может быть составным
				ts := strings.TrimPrefix(s, "-")
				tsa := strings.Split(ts, "")
				for _, ss := range tsa {
					args[ss] = ""
				}
			} else { // Типа, команда
				cmds = append(cmds, s)
			}
		}
	}
	return args, cmds

}

// ImplodeArgs -
func ImplodeArgs(Args map[string]string, Cmds []string) []string {
	ret := make([]string, 0)
	for _, s := range Cmds {
		ret = append(ret, s)
	}
	for k, v := range Args {
		if v == "" {
			if len(k) == 1 {
				ret = append(ret, "-"+k)
			} else {
				ret = append(ret, "--"+k)
			}
		} else {
			ret = append(ret, "--"+k+"=\""+v+"\"")
		}
	}
	return ret
}
