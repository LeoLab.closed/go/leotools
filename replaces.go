// Package leotools - replaces
package leotools

/*
	Допускаются замены:
		r[key]=string
		r[key]=function(key string) string
		r[key]=function() string
*/

import (
	"bytes"
	"fmt"
	"time"
)

// Replacer -
type Replacer struct {
	r map[string]interface{}
}

// NewReplacer -
func NewReplacer() *Replacer {
	return &Replacer{
		r: make(map[string]interface{}),
	}
}

// Replace -
func (R *Replacer) Replace(s string) string {
	b := bytes.NewBuffer([]byte(""))
	max := len(s)
	pi := 0
	key := ""
	for i := 0; i < max; {
		// Поиск начала ключа
		for i < max && s[i] != '%' {
			i++
		}
		b.WriteString(s[pi:i]) // Строка до ключа
		pi = i
		i++
		if i > max {
			return b.String() // Строка закончилась
		}
		// Поиск конца ключа
		for i < max && s[i] != '%' {
			i++
		}
		key = s[pi:i]
		i++
		pi = i
		if v, ok := R.r[key]; ok { // Ключ есть в массиве замен
			switch v.(type) {
			case string:
				b.WriteString(v.(string))
			case func(string) string:
				b.WriteString(v.(func(string) string)(key))
			case func() string:
				b.WriteString(v.(func() string)())
			default:
				// Неизвестный тип в замене
			}
		} else {
			// Ключа замены нет
		}
	}
	return b.String()
}

// Init -
func (R *Replacer) Init() {
	if R.r == nil {
		R.r = make(map[string]interface{})
	}
	R.r["date"] = func() string {
		return fmt.Sprintf("%0000d", time.Now().Year()) + "-" + fmt.Sprintf("%00d", time.Now().Month()) + "-" + fmt.Sprintf("%00d", time.Now().Day())
	}
	R.r["date.Y"] = func() string { return fmt.Sprintf("%0000d", time.Now().Year()) }
	R.r["date.M"] = func() string { return fmt.Sprintf("%00d", time.Now().Month()) }
	R.r["date.D"] = func() string { return fmt.Sprintf("%00d", time.Now().Day()) }
	R.r["time"] = func() string {
		return fmt.Sprintf("%00d", time.Now().Hour()) + ":" + fmt.Sprintf("%00d", time.Now().Minute()) + fmt.Sprintf("%00d", time.Now().Second())
	}
	R.r["time.H"] = func() string { return fmt.Sprintf("%00d", time.Now().Hour()) }
	R.r["time.M"] = func() string { return fmt.Sprintf("%00d", time.Now().Minute()) }
	R.r["time.S"] = func() string { return fmt.Sprintf("%00d", time.Now().Second()) }
	R.r["time.Z"] = func() string { return fmt.Sprintf("%000d", time.Now().Nanosecond()/1000) }
	R.r["time.ns"] = func() string { return fmt.Sprintf("%d", time.Now().Nanosecond()) }
}
