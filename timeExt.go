// Package leotools - timeExt
package leotools

import (
	"errors"
	"time"
)

// Константы измерения времени
const (
	Nanosecond  time.Duration = 1
	Microsecond               = 1000 * Nanosecond
	Millisecond               = 1000 * Microsecond
	Second                    = 1000 * Millisecond
	Minute                    = 60 * Second
	Hour                      = 60 * Minute
	Day                       = 24 * Hour
	Week                      = 7 * Day
	Month                     = 30 * Day
	Year                      = 365 * Day
)

var unitMap = map[string]int64{
	"ns": int64(Nanosecond),
	"us": int64(Microsecond),
	"µs": int64(Microsecond), // U+00B5 = micro symbol
	"μs": int64(Microsecond), // U+03BC = Greek letter mu
	"ms": int64(Millisecond),
	"s":  int64(Second),
	"m":  int64(Minute),
	"h":  int64(Hour),
	"D":  int64(Day),
	"W":  int64(Week),
	"M":  int64(Month),
	"Y":  int64(Year),
}

func parseDuration(s string) (d time.Duration, err error) {
	/*
		if s == "" {
			return 0, nil
		}
		orig := s
		neg := false
		var r int64 = 0
		if s[0] == '-' || s[0] == '+' {
			neg = s[0] == '-'
			s = s[1:]
		}
		var tr int64 = 0
		var n int64 = 1
		for _, c := range s {
			switch c {
			case '0':
				tr = 0 + tr*(10*n)
			case '1':
				tr = 1 + tr*(10*n)
			case '2':
				tr = 2 + tr*(10*n)
			case '3':
				tr = 3 + tr*(10*n)
			case '4':
				tr = 4 + tr*(10*n)
			case '5':
				tr = 5 + tr*(10*n)
			case '6':
				tr = 6 + tr*(10*n)
			case '7':
				tr = 7 + tr*(10*n)
			case '8':
				tr = 8 + tr*(10*n)
			case '9':
				tr = 9 + tr*(10*n)
			case '.':
				return 0, errors.New("Not supported float duration")
			default:
				/*
					if v, ok := unitMap[c]; ok {
						r = r + tr*unitMap[c]
						n = 0
					} else {
						return 0, errors.New("Invalid duration")
					}
				* /
			}
			n = n + 1
		}
	*/
	return 0, errors.New("Not yet ready")
}
