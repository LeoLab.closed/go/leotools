// Package leotools - logger
package leotools

import (
	"errors"
	"fmt"
	"log/syslog"
	"os"
	"runtime"
	//	"strings"
	"sync"
	"time"
)

// LogConf - конфигурация логгера
type LogConf struct {
	File    string
	Name    string
	RCDepth int
}

// Logger - основная структура
type Logger struct {
	depth int
	fName string
	fLog  *os.File
	sLog  *syslog.Writer
	m     sync.Mutex
}

// NewLogger - "конструктор" логгера
func NewLogger(c *LogConf) (Log *Logger, err error) {
	Log = &Logger{}
	if c.File != "" {
		Log.fLog, err = os.OpenFile(c.File, os.O_CREATE|os.O_APPEND|os.O_RDWR, 0666)
		if err != nil {
			return nil, errors.New(err.Error() + " (" + c.File + ")")
		}
		Log.fName = c.File
	}
	if c.Name != "" {
		Log.sLog, err = syslog.New(syslog.LOG_INFO, c.Name)
		if err != nil {
			return nil, errors.New(err.Error() + " (" + c.Name + ")")
		}
	}

	if c.RCDepth > 2 {
		Log.depth = c.RCDepth
	} else {
		Log.depth = 2
	}

	return Log, nil
	//	strings.NewReplacer()
}

// Close - Закрыть открытые файлы
func (L *Logger) Close() (err error) {
	if L.fLog != nil {
		err = L.fLog.Close()
		L.fLog = nil
	}
	if L.sLog != nil {
		err = L.sLog.Close()
		L.sLog = nil
	}
	return err
}

func (L *Logger) init() {}

func (L *Logger) write(t, s string) {
	if L.fLog == nil {
		return
	}
	n := time.Now()
	_, fn, ln, ok := runtime.Caller(L.depth)
	if !ok {
		fn = "???"
		ln = 0
	}
	ds := ""
	dt := ""
	{
		y, m, d := n.Date()
		ds = fmt.Sprintf("%0000d-%00d-%00d", y, m, d)
	}
	h, m, i := n.Clock()
	dt = fmt.Sprintf("%00d:%00d:%00d.%000000d", h, m, i, n.Nanosecond())
	L.m.Lock()
	defer L.m.Unlock()
	L.fLog.WriteString(ds + " " + dt + " [" + t + "] (" + fn + ":" + fmt.Sprintf("%d", ln) + ") " + s + "\n")

}

// Debug - запись
func (L *Logger) Debug(s string) {
	L.write("DBG", s)
	if L.sLog != nil {
		L.sLog.Debug(s)
	}
}

// Info - запись
func (L *Logger) Info(s string) {
	L.write("INFO", s)
	if L.sLog != nil {
		L.sLog.Info(s)
	}
}

// Warning - запись
func (L *Logger) Warning(s string) {
	L.write("WARN", s)
	if L.sLog != nil {
		L.sLog.Warning(s)
	}
}

// Err - запись
func (L *Logger) Err(s string) {
	L.write("ERROR", s)
	if L.sLog != nil {
		L.sLog.Err(s)
	}
}

// Crit - запись
func (L *Logger) Crit(s string) {
	L.write("CRITICAL", s)
	if L.sLog != nil {
		L.sLog.Crit(s)
	}
}
